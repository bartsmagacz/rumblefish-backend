import web3Controller from '../controlers/web3Controller';
import utils from '../utils/utils';

export default (expressApp, web3Obj, contract) => {
  expressApp.get('/web3/getBlockNumber', (req, res) => {
    web3Controller.web3getBlockNumber(web3Obj)
      .then((response) => {
        res.send(response.toString());
      })
      .catch((err) => {
        res.status(400);
        res.json(err);
      });
  });
  expressApp.get('/contract/getTransactionsNumber', (req, res) => {
    web3Controller
      .contractTransactionsNumber(web3Obj, contract, utils.numberOfBlocks(req.query.time))
      .then((response) => {
        res.send(response.toString());
      })
      .catch((err) => {
        res.status(400);
        res.json(err);
      });
  });
  expressApp.get('/contract/getTransferredAmount', (req, res) => {
    web3Controller
      .contractTransferredAmount(web3Obj, contract, utils.numberOfBlocks(req.query.time))
      .then((response) => {
        res.send(response.toString());
      })
      .catch((err) => {
        res.status(400);
        res.json(err);
      });
  });
};
