function numberOfBlocks(time) {
  switch (time) {
    case '1h':
      return 240;
    case '6h':
      return 1440;
    case '1d':
      return 5760;
    default:
      return 5760;
  }
}

export default {
  numberOfBlocks,
};
