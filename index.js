import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import Web3 from 'web3';
import abi from 'human-standard-token-abi';
import web3Routes from './routes/web3Routes';
import config from './config';

const app = express();
const port = process.env.PORT || 9876;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port);

console.log(`RESTful API server started on: ${port}`);

const web3 = new Web3();

web3.setProvider(new web3.providers.HttpProvider(config.rpcUrl));

const contract = new web3.eth.Contract(abi, '0xd850942ef8811f2a866692a623011bde52a462c1');

web3Routes(app, web3, contract);

export default app;
