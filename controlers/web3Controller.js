function web3getBlockNumber(web3Obj) {
  return new Promise((resolve, reject) => {
    web3Obj.eth.getBlockNumber((err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
}

async function contractTransactionsNumber(web3Obj, contract, blockNumbers) {
  const currentBlockNumber = await web3Obj.eth.getBlockNumber();
  const fromBlock = currentBlockNumber - blockNumbers;
  const transferEvents = await contract.getPastEvents('Transfer', {
    fromBlock,
    filter: {
      isError: 0,
      txreceipt_status: 1,
    },
    topics: [
      web3Obj.utils.sha3('Transfer(address,address,uint256)'),
    ],
  });
  return transferEvents.length;
}

async function contractTransferredAmount(web3Obj, contract, blockNumbers) {
  const currentBlockNumber = await web3Obj.eth.getBlockNumber();
  const fromBlock = currentBlockNumber - blockNumbers;
  const transferEvents = await contract.getPastEvents('Transfer', {
    fromBlock,
    filter: {
      isError: 0,
      txreceipt_status: 1,
    },
    topics: [
      web3Obj.utils.sha3('Transfer(address,address,uint256)'),
    ],
  });
  let transferredAmount = 0;
  // TODO - rework to reduce
  transferEvents.forEach(({ returnValues }) => {
    transferredAmount += returnValues._value * (10 ** -18);
  });
  return transferredAmount;
}

export default {
  web3getBlockNumber,
  contractTransactionsNumber,
  contractTransferredAmount,
};
